$(document).ready(function() {
    $('#mycarousel').carousel({
        interval: 2000
    });
    $("#carouselButton").click(function() {
        if ($("#carouselButton").children("span").hasClass('fa-pause')) {
            $("#mycarousel").carousel('pause');
            $("#carouselButton").children("span").removeClass('fa-pause');
            $("#carouselButton").children("span").addClass('fa-play');
        } else if ($("#carouselButton").children("span").hasClass('fa-play')) {
            $("#mycarousel").carousel('cycle');
            $("#carouselButton").children("span").removeClass('fa-play');
            $("#carouselButton").children("span").addClass('fa-pause');
        }
    });


    $("#loginLink").click(function() {
        $('#loginModal').modal('toggle');
    });

    $("#reserverTableLink").click(function() {
        $('#reserved').modal('toggle');
    });
    $('.close').click(function() {
        $('#loginModal').modal('hide')
    })
    $('.close').click(function() {
        $('#reserved').modal('hide')
    })
});
$('.btn-cancle').click(function() {
    $('#loginModal').modal('hide')
})
$('.btn-cancle-2').click(function() {
    $('#reserved').modal('hide')
})